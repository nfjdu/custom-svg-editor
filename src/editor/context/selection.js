import { Svg, G } from '@svgdotjs/svg.js';
import '@svgdotjs/svg.draggable.js';
import '../svgjs-extensions/selection';
import {
  ElementSelectedEvent,
  ElementUnselectedEvent,
  SelectionChangedEvent,
  SelectionChangedEventArgs,
} from '../events/selection';
export class SelectionContext {
  constructor() {
    /**
     * @private
     * @type {Array<Svg>}
     */
    this.selectedSvgElements = [];
    this.elementSelectedEvent = new ElementSelectedEvent();
    this.elementUnselectedEvent = new ElementUnselectedEvent();
    this.selectionChangedEvent = new SelectionChangedEvent();
  }

  /**
   *
   * @param {Svg} svgElement
   */
  addElementToSelection(svgElement) {
    this.selectedSvgElements.push(svgElement);
    console.log(
      'file: selection.js ~ line 23 ~ SelectionContext ~ addElementToSelection ~ this.selectedSvgElements',
      this.selectedSvgElements
    );
    //fire event that selection changed
    this.selectionChangedEvent.fire(
      new SelectionChangedEventArgs(this.selectedSvgElements)
    );
  }
  /**
   *
   * @param {Svg} svgElement
   */
  removeElementFromSelection(svgElement) {
    this.selectedSvgElements = this.selectedSvgElements.filter(
      (el) => el !== svgElement
    );
    console.log(
      'file: selection.js ~ line 23 ~ SelectionContext ~ removeElementFromSelection ~ this.selectedSvgElements',
      this.selectedSvgElements
    );
    //fire event that selection changed
    this.selectionChangedEvent.fire(
      new SelectionChangedEventArgs(this.selectedSvgElements)
    );
  }

  changeSelectedElementClass(newClass) {
    if (this.selectedSvgElements.length === 1)
      this.selectedSvgElements[0].node.setAttribute('class', newClass);
  }

  groupSelectedElements() {
    const group = new G();
    const { elementSelectedEvent, elementUnselectedEvent } = this;

    this.selectedSvgElements.forEach((element) => {
      element.draggable(false);
      element.unselect();
      element.selection({ selectable: false });
      group.add(element);
    });

    group
      .draggable(true)
      .selection({
        selectable: true,
        elementSelectedEvent,
        elementUnselectedEvent,
      })
      .select();

    return group;
  }

  ungroupSelectedGroup() {
    const { elementSelectedEvent, elementUnselectedEvent } = this;

    if (
      this.selectedSvgElements.length === 1 &&
      this.selectedSvgElements[0] instanceof G
    ) {
      this.selectedSvgElements[0].children().forEach((el) =>
        el
          .draggable(true)
          .selection({
            selectable: true,
            elementSelectedEvent,
            elementUnselectedEvent,
          })
          .select()
      );
      this.selectedSvgElements[0].unselect().ungroup();
    }
  }

  clearSelection() {
    this.selectedSvgElements.forEach((element) => {
      element.unselect();
    });
  }
}
