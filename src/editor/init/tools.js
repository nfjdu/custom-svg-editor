import { TOOLS } from '../constants/tools';

/**
 *
 * @param {import('./initContexts').Contexts} contexts
 */
export function toolsContextPreconfigurations({ toolsContext }) {
  // choose selection tool as current by default
  toolsContext.selectCurrentTool(TOOLS.SELECTION);
}
