import { G } from '@svgdotjs/svg.js';
import { TOOLS } from '../../constants/tools';
import { updateLayersList } from '../../helpers/ui/layers';
import { updateUiOnToolChanged } from '../../helpers/ui/tools';

/**
 *
 * @param {import('../initContexts').Contexts} contexts
 */
export function configureUIContextSubscriptions({
  toolsContext,
  layersContext,
  selectionContext,
}) {
  // update selected tool on UI when tool changes
  toolsContext.toolChangedEvent.addHandler((args) => {
    updateUiOnToolChanged(args.tool);
  });

  // update layers list on layers data change
  layersContext.layersChanged.addHandler((args) => {
    selectionContext.clearSelection();

    updateLayersList(args.layersNames, args.currentLayerName, (layerName) => {
      layersContext.selectCurrentLayer(layerName);
      if (toolsContext.getCurrentTool() === TOOLS.SELECTION)
        layersContext.enableLayer(layerName);
    });
  });

  // show or hide buttons for selected item
  selectionContext.selectionChangedEvent.addHandler((args) => {
    const elemClassContainer = document.getElementById(
      'single-selected-elem-attributes'
    );
    const groupElementsButton = document.getElementById('group-elements-tool');
    const ungroupElementsButton = document.getElementById(
      'ungroup-elements-tool'
    );

    if (args.selectedSvgElements.length === 1) {
      elemClassContainer.style.display = 'flex';

      // update class input value, according to selected item
      const classNameInput = document.getElementById(
        'selected-elem-classname-input'
      );
      classNameInput.value =
        args.selectedSvgElements[0].node.getAttribute('class') || '';

      groupElementsButton.style.display = 'none';
      if (args.selectedSvgElements[0] instanceof G) {
        ungroupElementsButton.style.display = 'flex';
      }
    } else if (args.selectedSvgElements.length > 1) {
      elemClassContainer.style.display = 'none';

      groupElementsButton.style.display = 'flex';
      ungroupElementsButton.style.display = 'none';
    } else {
      elemClassContainer.style.display = 'none';

      groupElementsButton.style.display = 'none';
      ungroupElementsButton.style.display = 'none';
    }
  });
}
