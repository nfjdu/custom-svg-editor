import { DataEvent, EventArgs } from './event';

export class ToolChangedEvent extends DataEvent {
  /**
   *
   * @callback ToolChangedEventHandler
   * @param {ToolChangedEventArgs} eventArgs
   */

  /**
   *
   * @param {ToolChangedEventHandler} handler - A callback to run.
   */
  addHandler(handler) {
    super.addHandler(handler);
  }

  /**
   *
   * @param {ToolChangedEventArgs} eventArgs
   */
  fire(eventArgs) {
    super.fire(eventArgs);
  }
}

export class ToolChangedEventArgs extends EventArgs {
  constructor(tool = '') {
    super();
    this.tool = tool;
  }
}
