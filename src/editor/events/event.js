export class DataEvent {
  constructor() {
    /**
     * @private
     */
    this.handlers = [];
  }

  addHandler(callback) {
    this.handlers.push(callback);
  }

  fire(eventArgs) {
    this.handlers.forEach((handler) => {
      handler(eventArgs);
    });
  }
}

export class EventArgs {}
